package com.devcamp.rest_api.CustomerAccountAPI;

public class Account {
    int id;
    Customer customer;
    double balance = 0.0;

    public Account(int id, Customer customer, double balance) {
        this.id = id;
        this.customer = customer;
        this.balance = balance;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

  

    public String getCustomerName(String name) {
        return name;
    }

    public void deposit(double amount) {
        this.balance += amount;
    }

    public void withdraw(double amount) {
        if (balance >= amount)
            balance -= amount;
        else
            System.out.println("amount withdrawn exceeds the current balance!");
       
    }
    @Override
    public String toString() {
        return "Account [name = " + customer.getName() + ", id=" + id + ", balance=" + balance + "]";
    }
}
