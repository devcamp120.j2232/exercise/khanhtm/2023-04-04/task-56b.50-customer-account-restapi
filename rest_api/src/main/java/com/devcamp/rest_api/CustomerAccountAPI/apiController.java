package com.devcamp.rest_api.CustomerAccountAPI;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class apiController {

    @GetMapping("/accounts")
    public ArrayList<Account> name() {
        Customer customer1 = new Customer(1, "Trần Minh Khanh", 30);
        Customer customer2 = new Customer(2, "Hoàng Nhọc Minh", 20);
        Customer customer3 = new Customer(3, "Lê My", 20);
        Customer customer4 = new Customer(4, "Khôn Ngọc", 50);
        Account account1 = new Account(1, customer1, 200000);
        Account account2 = new Account(2, customer2, 300000);
        Account account3 = new Account(3, customer3, 400000);
        Account account4 = new Account(4, customer4, 500000);
        ArrayList<Account> account = new ArrayList<Account>();
        account.add(account1);
        account.add(account2);
        account.add(account3);
        account.add(account4);
        return account;
    }
}
